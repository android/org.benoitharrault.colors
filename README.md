# Colors, a color flood fill game

## Your mission

Fill the board with only one color.

## How to play

### Start game

-   Select game settings (difficulty, board size, colors count and skin)
-   Hit play button to generate a randomly colored board

### Play

-   Pick color from palette, all touching cells from top left with the same color will be filled with this selected color
-   Repeat until all board is filled

### Victory

-   If you can fill board with less moves count than show target, you win!

## Credits

This game is heavily inspired from [Open Flood](https://f-droid.org/en/packages/com.gunshippenguin.openflood/), a simple but addictive flood fill game, available on [F-Droid repository](https://f-droid.org/en/) ([source code](https://github.com/GunshipPenguin/open_flood)).

This application is built with Flutter framework.
