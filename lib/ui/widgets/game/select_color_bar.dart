import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/config/application_config.dart';
import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/cell.dart';
import 'package:colors/models/activity/activity.dart';
import 'package:colors/ui/widgets/game/cell.dart';

class SelectColorBar extends StatelessWidget {
  const SelectColorBar({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final int colorsCount = currentActivity.activitySettings
            .getAsInt(ApplicationConfig.parameterCodeColorsCount);
        final double blockWidth = MediaQuery.of(context).size.width;
        final double itemWidth = blockWidth / colorsCount;

        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            for (int value = 1; value <= colorsCount; value++)
              CellWidget(
                cell: Cell(value),
                itemWidth: itemWidth,
              ),
          ],
        );
      },
    );
  }
}
