import 'package:colors/config/application_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/activity.dart';
import 'package:colors/ui/painters/board_painter.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;
        final double boardWidth = MediaQuery.of(context).size.width;

        return GestureDetector(
          onTapUp: (details) {
            final double xTap = details.localPosition.dx;
            final double yTap = details.localPosition.dy;
            final int boardSize = currentActivity.activitySettings
                .getAsInt(ApplicationConfig.parameterCodeBoardSize);
            final int col = xTap ~/ (boardWidth / boardSize);
            final int row = yTap ~/ (boardWidth / boardSize);
            final int cellValue = currentActivity.board.getCellValue(col, row);

            BlocProvider.of<ActivityCubit>(context).fillBoardFromFirstCell(cellValue);
          },
          child: CustomPaint(
            size: Size(boardWidth, boardWidth),
            willChange: false,
            painter: BoardPainter(
              activity: currentActivity,
              colorScheme: Theme.of(context).colorScheme,
            ),
            isComplex: true,
          ),
        );
      },
    );
  }
}
