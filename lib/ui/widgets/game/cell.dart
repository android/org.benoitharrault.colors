import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/config/application_config.dart';
import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/cell.dart';
import 'package:colors/models/activity/activity.dart';
import 'package:colors/utils/color_theme_utils.dart';

class CellWidget extends StatelessWidget {
  const CellWidget({
    super.key,
    required this.cell,
    required this.itemWidth,
  });

  final Cell cell;
  final double itemWidth;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final ColorScheme colorScheme = Theme.of(context).colorScheme;

        final String colorTheme =
            currentActivity.activitySettings.get(ApplicationConfig.parameterCodeColorTheme);
        final squareColor = Color(ColorThemeUtils.getColorCode(cell.value, colorTheme));

        return Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: colorScheme.onSurface,
              width: 4,
            ),
          ),
          child: GestureDetector(
            child: Container(
              color: squareColor,
              padding: const EdgeInsets.all(3),
              child: SizedBox.square(
                dimension: itemWidth - 19,
              ),
            ),
            onTap: () {
              if (!currentActivity.animationInProgress &&
                  currentActivity.board.getFirstCellValue() != cell.value) {
                BlocProvider.of<ActivityCubit>(context).fillBoardFromFirstCell(cell.value);
              }
            },
          ),
        );
      },
    );
  }
}
