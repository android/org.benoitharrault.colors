import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/activity.dart';

class GameIndicatorMaxMovesCount extends StatelessWidget {
  const GameIndicatorMaxMovesCount({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        Color maxMovesCountColor = Colors.grey;
        if (currentActivity.movesCount > currentActivity.maxMovesCount) {
          maxMovesCountColor = Colors.red;
        }

        return Text(
          '(max: ${currentActivity.maxMovesCount})',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w600,
            color: maxMovesCountColor,
          ),
        );
      },
    );
  }
}
