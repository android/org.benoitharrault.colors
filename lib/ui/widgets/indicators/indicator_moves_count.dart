import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/activity.dart';

class GameIndicatorMovesCount extends StatelessWidget {
  const GameIndicatorMovesCount({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Text(
          currentActivity.movesCount.toString(),
          style: TextStyle(
            fontSize: 35,
            fontWeight: FontWeight.w600,
            color: Theme.of(context).colorScheme.primary,
          ),
        );
      },
    );
  }
}
