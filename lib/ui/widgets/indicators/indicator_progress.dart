import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/activity.dart';

class GameIndicatorProgress extends StatelessWidget {
  const GameIndicatorProgress({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        String progressIndicator =
            '${currentActivity.progress}/${currentActivity.progressTotal}';

        if (currentActivity.movesCount > 0) {
          progressIndicator += ' (+${currentActivity.progressDelta})';
        }

        return Text(
          progressIndicator,
          style: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w600,
            color: Colors.green,
          ),
        );
      },
    );
  }
}
