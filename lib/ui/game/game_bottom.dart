import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/cubit/activity/activity_cubit.dart';
import 'package:colors/models/activity/activity.dart';
import 'package:colors/ui/widgets/game/select_color_bar.dart';

class GameBottomWidget extends StatelessWidget {
  const GameBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return !currentActivity.isFinished ? const SelectColorBar() : const SizedBox.shrink();
      },
    );
  }
}
