import 'package:flutter/material.dart';

import 'package:colors/ui/widgets/indicators/indicator_max_moves.dart';
import 'package:colors/ui/widgets/indicators/indicator_moves_count.dart';
import 'package:colors/ui/widgets/indicators/indicator_progress.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Table(
      children: const [
        TableRow(
          children: [
            Column(
              children: [
                GameIndicatorMovesCount(),
              ],
            ),
            Column(
              children: [
                GameIndicatorMaxMovesCount(),
                GameIndicatorProgress(),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
