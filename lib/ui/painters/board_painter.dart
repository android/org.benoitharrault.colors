import 'package:flutter/material.dart';

import 'package:colors/config/application_config.dart';
import 'package:colors/models/activity/board.dart';
import 'package:colors/models/activity/cell.dart';
import 'package:colors/models/activity/activity.dart';
import 'package:colors/utils/color_theme_utils.dart';

class BoardPainter extends CustomPainter {
  const BoardPainter({
    required this.activity,
    required this.colorScheme,
  });

  final Activity activity;
  final ColorScheme colorScheme;

  @override
  void paint(Canvas canvas, Size size) {
    final int boardSize =
        activity.activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final BoardCells cells = activity.board.cells;
    final double cellSize = size.width / boardSize;
    const double borderSize = 3;

    // background
    for (var row = 0; row < boardSize; row++) {
      final double y = cellSize * row;
      for (var col = 0; col < boardSize; col++) {
        final double x = cellSize * col;

        final Cell cell = cells[row][col];
        final int cellValue = cell.value;
        final int colorCode = ColorThemeUtils.getColorCode(cellValue,
            activity.activitySettings.get(ApplicationConfig.parameterCodeColorTheme));

        final cellPaintBackground = Paint();
        cellPaintBackground.color = Color(colorCode);
        cellPaintBackground.style = PaintingStyle.fill;

        final Rect cellBackground = Rect.fromPoints(
            Offset(x - (borderSize / 2), y - (borderSize / 2)),
            Offset(x + (borderSize / 2) + cellSize, y + (borderSize / 2) + cellSize));

        canvas.drawRect(cellBackground, cellPaintBackground);
      }
    }

    // borders
    final cellPaintBorder = Paint();
    cellPaintBorder.color = colorScheme.onSurface;
    cellPaintBorder.strokeWidth = borderSize;
    cellPaintBorder.strokeCap = StrokeCap.round;

    for (var row = 0; row < boardSize; row++) {
      final double y = cellSize * row;
      for (var col = 0; col < boardSize; col++) {
        final double x = cellSize * col;

        final Cell cell = cells[row][col];
        final int cellValue = cell.value;

        if ((row == 0) ||
            (row > 1 && cellValue != activity.board.getCellValue(col, row - 1))) {
          final Offset borderStart = Offset(x, y);
          final Offset borderStop = Offset(x + cellSize, y);
          canvas.drawLine(borderStart, borderStop, cellPaintBorder);
        }
        if ((row == boardSize - 1) ||
            ((row + 1) < boardSize &&
                cellValue != activity.board.getCellValue(col, row + 1))) {
          final Offset borderStart = Offset(x, y + cellSize);
          final Offset borderStop = Offset(x + cellSize, y + cellSize);
          canvas.drawLine(borderStart, borderStop, cellPaintBorder);
        }
        if ((col == 0) ||
            (col > 1 && cellValue != activity.board.getCellValue(col - 1, row))) {
          final Offset borderStart = Offset(x, y);
          final Offset borderStop = Offset(x, y + cellSize);
          canvas.drawLine(borderStart, borderStop, cellPaintBorder);
        }
        if ((col == boardSize - 1) ||
            ((col + 1) < boardSize &&
                cellValue != activity.board.getCellValue(col + 1, row))) {
          final Offset borderStart = Offset(x + cellSize, y);
          final Offset borderStop = Offset(x + cellSize, y + cellSize);
          canvas.drawLine(borderStart, borderStop, cellPaintBorder);
        }
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
