class Cell {
  Cell(
    this.value,
  );

  int value;

  @override
  String toString() {
    return '$Cell(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'value': value,
    };
  }
}
