import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/config/application_config.dart';
import 'package:colors/models/activity/cell.dart';

typedef BoardCells = List<List<Cell>>;

class Board {
  final BoardCells cells;

  Board({
    required this.cells,
  });

  factory Board.createEmpty() {
    return Board(cells: []);
  }

  factory Board.createRandom(ActivitySettings activitySettings) {
    final int boardSizeHorizontal =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);

    final rand = Random();

    BoardCells cells = [];
    for (int rowIndex = 0; rowIndex < boardSizeVertical; rowIndex++) {
      List<Cell> row = [];
      for (int colIndex = 0; colIndex < boardSizeHorizontal; colIndex++) {
        final int value = 1 +
            rand.nextInt(
                activitySettings.getAsInt(ApplicationConfig.parameterCodeColorsCount));
        row.add(Cell(value));
      }
      cells.add(row);
    }

    return Board(
      cells: cells,
    );
  }

  int getCellValue(int col, int row) {
    return cells[row][col].value;
  }

  int getFirstCellValue() {
    return cells[0][0].value;
  }

  List<List<int>> getSiblingFillableCells(
    int row,
    int col,
    List<List<int>> siblingCells,
  ) {
    final int boardSize = cells.length;

    if (siblingCells.isEmpty) {
      siblingCells = [
        [row, col]
      ];
    }

    final int referenceValue = cells[row][col].value;

    for (int deltaRow = -1; deltaRow <= 1; deltaRow++) {
      for (int deltaCol = -1; deltaCol <= 1; deltaCol++) {
        if (deltaCol == 0 || deltaRow == 0) {
          final int candidateRow = row + deltaRow;
          final int candidateCol = col + deltaCol;

          if ((candidateRow >= 0 && candidateRow < boardSize) &&
              (candidateCol >= 0 && candidateCol < boardSize)) {
            if (cells[candidateRow][candidateCol].value == referenceValue) {
              bool alreadyFound = false;
              for (int index = 0; index < siblingCells.length; index++) {
                if ((siblingCells[index][0] == candidateRow) &&
                    (siblingCells[index][1] == candidateCol)) {
                  alreadyFound = true;
                }
              }
              if (!alreadyFound) {
                siblingCells.add([candidateRow, candidateCol]);
                siblingCells = getSiblingFillableCells(
                  candidateRow,
                  candidateCol,
                  siblingCells,
                );
              }
            }
          }
        }
      }
    }

    return siblingCells;
  }

  // check grid contains only one color
  bool isBoardSolved() {
    final int firstCellValue = cells[0][0].value;
    for (int row = 0; row < cells.length; row++) {
      for (int col = 0; col < cells[row].length; col++) {
        if (cells[row][col].value != firstCellValue) {
          return false;
        }
      }
    }

    printlog('-> ok grid fully painted!');

    return true;
  }

  void dump() {
    String horizontalRule = '----';
    for (int i = 0; i < cells[0].length; i++) {
      horizontalRule += '-';
    }

    printlog('Board:');
    printlog(horizontalRule);

    for (int rowIndex = 0; rowIndex < cells.length; rowIndex++) {
      String row = '| ';
      for (int colIndex = 0; colIndex < cells[rowIndex].length; colIndex++) {
        row += cells[rowIndex][colIndex].value.toString();
      }
      row += ' |';

      printlog(row);
    }

    printlog(horizontalRule);
  }

  @override
  String toString() {
    return '$Board(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'cells': cells,
    };
  }
}
