import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/config/application_config.dart';
import 'package:colors/models/activity/board.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.board,

    // Game data
    this.maxMovesCount = 0,
    this.movesCount = 0,
    this.progress = 0,
    this.progressTotal = 0,
    this.progressDelta = 0,
    this.gameWon = false,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  final Board board;

  // Game data
  int maxMovesCount;
  int movesCount;
  int progress;
  int progressTotal;
  int progressDelta;
  bool gameWon;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: Board.createEmpty(),
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final int baseMaxMovesCount = (30 *
            (newActivitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize) *
                newActivitySettings.getAsInt(ApplicationConfig.parameterCodeColorsCount)) /
            (17 * 6))
        .round();
    final int deltaMovesCountFromLevel =
        ApplicationConfig.getMovesCountLimitDeltaFromLevelCode(
            newActivitySettings.get(ApplicationConfig.parameterCodeDifficultyLevel));

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      board: Board.createRandom(newActivitySettings),
      // Game data
      progress: 1,
      progressTotal: newActivitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize) *
          newActivitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize),
      maxMovesCount: baseMaxMovesCount + deltaMovesCountFromLevel,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    activitySettings.dump();
    printlog('$Activity:');
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('    canBeResumed: $canBeResumed');
    printlog('  Base data');
    board.dump();
    printlog('  Game data');
    printlog('    maxMovesCount: $maxMovesCount');
    printlog('    movesCount: $movesCount');
    printlog('    progress: $progress');
    printlog('    progressTotal: $progressTotal');
    printlog('    progressDelta: $progressDelta');
    printlog('    canBeResumed: $canBeResumed');
    printlog('    gameWon: $gameWon');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'board': board.toJson(),
      // Game data
      'maxMovesCount': maxMovesCount,
      'movesCount': movesCount,
      'progress': progress,
      'progressTotal': progressTotal,
      'progressDelta': progressDelta,
      'gameWon': gameWon,
    };
  }
}
