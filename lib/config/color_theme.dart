import 'package:colors/config/application_config.dart';

class ColorTheme {
  static const Map<String, List<int>> colorThemes = {
    ApplicationConfig.colorThemeDefault: [
      0xffffff,
      0xe63a3f,
      0x708cfd,
      0x359c35,
      0xffce2c,
      0xff6f43,
      0xa13cb1,
      0x38ffff,
      0xf2739d,
    ],

    // https://lospec.com/palette-list/sweethope
    ApplicationConfig.colorThemeSweethope: [
      0xffffff,
      0x615e85,
      0x9c8dc2,
      0xd9a3cd,
      0xebc3a7,
      0xe0e0dc,
      0xa3d1af,
      0x90b4de,
      0x717fb0,
    ],

    // https://lospec.com/palette-list/nostalgic-dreams
    ApplicationConfig.colorThemeNostalgicDreams: [
      0xffffff,
      0xd9af80,
      0xb07972,
      0x524352,
      0x686887,
      0x7f9bb0,
      0xbfd4b0,
      0x90b870,
      0x628c70,
    ],

    // https://lospec.com/palette-list/arjibi8
    ApplicationConfig.colorThemeArjibi8: [
      0xffffff,
      0x8bc7bf,
      0x5796a1,
      0x524bb3,
      0x471b6e,
      0x702782,
      0xb0455a,
      0xde8b6f,
      0xebd694,
    ],
  };

  static const int defaultThemeColor = 0x808080;
}
