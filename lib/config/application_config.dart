import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/cubit/activity/activity_cubit.dart';

import 'package:colors/ui/pages/game.dart';

import 'package:colors/ui/parameters/parameter_painter_board_size.dart';
import 'package:colors/ui/parameters/parameter_painter_color_theme.dart';
import 'package:colors/ui/parameters/parameter_painter_colors_count.dart';
import 'package:colors/ui/parameters/parameter_painter_difficulty_level.dart';

class ApplicationConfig {
  // activity parameter: difficulty level values
  static const String parameterCodeDifficultyLevel = 'activity.difficultyLevel';
  static const String difficultyLevelValueEasy = 'easy';
  static const String difficultyLevelValueMedium = 'medium';
  static const String difficultyLevelValueHard = 'hard';
  static const String difficultyLevelValueNightmare = 'nightmare';

  // activity parameter: board size values
  static const String parameterCodeBoardSize = 'activity.boardSize';
  static const String boardSizeValueSmall = 'small';
  static const String boardSizeValueMedium = 'medium';
  static const String boardSizeValueLarge = 'large';
  static const String boardSizeValueExtraLarge = 'extra';

  // activity parameter: colors count values
  static const String parameterCodeColorsCount = 'activity.colorsCount';
  static const String colorsCountValueLow = '5';
  static const String colorsCountValueMedium = '6';
  static const String colorsCountValueHigh = '7';
  static const String colorsCountValueVeryHigh = '8';

  // activity parameter: colors theme values
  static const String parameterCodeColorTheme = 'global.colorTheme';
  static const String colorThemeDefault = 'default';
  static const String colorThemeSweethope = 'sweethope';
  static const String colorThemeNostalgicDreams = 'nostalgic-dreams';
  static const String colorThemeArjibi8 = 'arjibi8';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Colors',
    activitySettings: [
      // difficulty level
      ApplicationSettingsParameter(
        code: parameterCodeDifficultyLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueEasy,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueHard,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueNightmare,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterDifficultyLevel(context: context, value: value);
        },
      ),

      // board size
      ApplicationSettingsParameter(
        code: parameterCodeBoardSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueSmall,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueLarge,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueExtraLarge,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterBoardSize(value: value, context: context);
        },
        intValueGetter: (String value) {
          const Map<String, int> intValues = {
            boardSizeValueSmall: 6,
            boardSizeValueMedium: 10,
            boardSizeValueLarge: 14,
            boardSizeValueExtraLarge: 20,
          };
          return intValues[value] ?? 0;
        },
      ),

      // colors count
      ApplicationSettingsParameter(
        code: parameterCodeColorsCount,
        values: [
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueLow,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueHigh,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountValueVeryHigh,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterColorsCount(value: value, context: context);
        },
      ),

      // colors theme
      ApplicationSettingsParameter(
        code: parameterCodeColorTheme,
        displayedOnTop: false,
        values: [
          ApplicationSettingsParameterItemValue(
            value: colorThemeDefault,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorThemeSweethope,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorThemeNostalgicDreams,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorThemeArjibi8,
          ),
        ],
        customPainter: (context, value) {
          return ParameterPainterColorTheme(value: value, context: context);
        },
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );

  static int getMovesCountLimitDeltaFromLevelCode(String parameterLevel) {
    const Map<String, int> values = {
      difficultyLevelValueEasy: 5,
      difficultyLevelValueMedium: 3,
      difficultyLevelValueHard: 1,
      difficultyLevelValueNightmare: -1,
    };
    return values[parameterLevel] ??
        getMovesCountLimitDeltaFromLevelCode(ApplicationConfig.config
            .getFromCode(ApplicationConfig.parameterCodeDifficultyLevel)
            .defaultValue);
  }
}
