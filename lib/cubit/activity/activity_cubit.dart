import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:colors/models/activity/activity.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createEmpty(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      // Base data
      board: state.currentActivity.board,
      // Game data
      maxMovesCount: state.currentActivity.maxMovesCount,
      movesCount: state.currentActivity.movesCount,
      progress: state.currentActivity.progress,
      progressTotal: state.currentActivity.progressTotal,
      progressDelta: state.currentActivity.progressDelta,
      gameWon: state.currentActivity.gameWon,
    );
    // activity.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    updateGameIsRunning(true);

    refresh();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    updateGameIsRunning(false);
    updateGameIsFinished(true);
    refresh();
  }

  void updateCellValue(int col, int row, int value) {
    state.currentActivity.board.cells[row][col].value = value;
    refresh();
  }

  void increaseMovesCount() {
    state.currentActivity.movesCount++;
    refresh();
  }

  void updateProgressDelta(int progressDelta) {
    state.currentActivity.progressDelta = progressDelta;
    refresh();
  }

  void updateProgress(int progress) {
    state.currentActivity.progress = progress;
    refresh();
  }

  void updateAnimationInProgress(bool inProgress) {
    state.currentActivity.animationInProgress = inProgress;
    refresh();
  }

  void updateGameIsRunning(bool gameIsRunning) {
    state.currentActivity.isRunning = gameIsRunning;
    refresh();
  }

  void updateGameIsStarted(bool gameIsStarted) {
    state.currentActivity.isStarted = gameIsStarted;
    refresh();
  }

  void updateGameIsFinished(bool gameIsFinished) {
    state.currentActivity.isFinished = gameIsFinished;
    refresh();
  }

  void updateGameWon(bool gameWon) {
    state.currentActivity.gameWon = gameWon;
    refresh();
  }

  fillBoardFromFirstCell(int value) {
    updateGameIsStarted(true);

    List<List<int>> cellsToFill =
        state.currentActivity.board.getSiblingFillableCells(0, 0, []);
    final int progressBeforeMove = cellsToFill.length;

    if (value == state.currentActivity.board.getFirstCellValue()) {
      return;
    }

    increaseMovesCount();

    // Sort cells from the closest to the furthest, relatively to the top left corner
    cellsToFill
        .sort((a, b) => (pow(a[0], 2) + pow(a[1], 2)).compareTo(pow(b[0], 2) + pow(b[1], 2)));

    const interval = Duration(milliseconds: 10);
    int cellIndex = 0;
    updateAnimationInProgress(true);
    Timer.periodic(
      interval,
      (Timer timer) {
        if (cellIndex < cellsToFill.length) {
          updateCellValue(cellsToFill[cellIndex][1], cellsToFill[cellIndex][0], value);
          cellIndex++;
        } else {
          timer.cancel();

          int progressAfterMove =
              state.currentActivity.board.getSiblingFillableCells(0, 0, []).length;
          int progressDelta = progressAfterMove - progressBeforeMove;
          updateProgressDelta(progressDelta);
          updateProgress(progressAfterMove);

          updateAnimationInProgress(false);

          if (state.currentActivity.board.isBoardSolved()) {
            updateGameWon(true);
            updateGameIsFinished(true);
          }
        }
      },
    );
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
